<?php  
include_once('simple_html_dom.php');

class WebParser {

	private $DOM;
	private $URL;
	private $CATEGORY;

	public function __construct($url, $category)
	{
		// $html = file_get_html('http://qafqazinfo.az/news/detail/2-manata-istirahet-fotolar-222879');
		$html = file_get_html($url);
		$this->DOM =  $html->find(".news_text")[0];
		$this->URL = $url;
		$this->CATEGORY = $category;
	}

	public function title()
	{
		$txt = $this->DOM->find("h2")[0]->plaintext;
		$txt = str_replace(" - Fotolar", "", $txt);
		$txt = str_replace(" - Video", "", $txt);
		return trim($txt);
	}

	public function date()
	{
		return str_replace("| ", "", trim($this->DOM->find("time")[0]->getAttribute("datetime")));	
	}

	public function images()
	{
		$images =  $this->DOM->find("img");
		$arr = array();
		foreach ($images as $value) {
			if(preg_match('/qafqazinfo/',$value->src)){
				array_push($arr, $value->src);			
			}
		}
		return $arr;
	}

	public function videos()
	{
		$videos =  $this->DOM->find("iframe");
		$arr = array();
		foreach ($videos as $value) {
			array_push($arr, str_replace("//", "", $value->src));
		}
		return $arr;
	}

	public function category()
	{
		return $this->CATEGORY;
	}

	public function url()
	{
		return $this->URL;
	}

	public function stats()
	{
		$txt = $this->text();
		$word_count = $this->word_count($txt);
		$sentence_count = count(preg_split('/[\.(\.\.\.)\?\!]/', $txt));
		$most_frequent_word = $this->most_frequent_word($txt);
		return array($word_count, $sentence_count, $most_frequent_word);
	}

	public function text()
	{
		$rawText =  $this->DOM->plaintext;

		$matches = array();

		preg_match('/Oxunma sayı: [0-9]+\K(.*)/', $rawText, $matches);

		$pos = str_replace("Daha tez məlumatlanmaq üçün yeni Facebook səhifəmizi", "", $matches[0]);
		$pos = strip_tags($pos);
		$pos = preg_replace('/\s+/', ' ', $pos);
		return trim($pos);
	}

	private function word_count($value)
	{
		return count(explode(" ", trim($value)))+1;	
	}

	private function most_frequent_word($value)
	{
		// $freq = array();
		// $matches = array();
		// preg_match_all('~\w+(?:-\w+)*~', $value, $matches);
		// foreach ($matches[0] as $word) {
		// 	$word = trim($word);
		// 	if(!in_array($word, $freq)){
		// 		$freq[$word] = 1;
		// 	}else{
		// 		$freq[$word]++;
		// 	}
		// }
		// arsort($freq);
		// return $freq;
		return "";
	}

}

