<?php
include_once('connection.php');
include_once('web_parser.php');  
include_once('simple_html_dom.php');

$categories = array(
	"xəbər" => "http://qafqazinfo.az/news/category/xeber-1",
	"siyasət" => "http://qafqazinfo.az/news/category/siyaset-2",
	"sosial" => "http://qafqazinfo.az/news/category/sosial-3",
	"iqtisadiyyat" => "http://qafqazinfo.az/news/category/iqtisadiyyat-4",
	"kriminal" => "http://qafqazinfo.az/news/category/kriminal-5",
	"dünya" => "http://qafqazinfo.az/news/category/dunya-6",
	"qafqaz" => "http://qafqazinfo.az/news/category/qafqaz-7",
	"idman" => "http://qafqazinfo.az/news/category/idman-9",
	"şou" => "http://qafqazinfo.az/news/category/sou-20"
);

$urls = array();
$wpos = array();

foreach($categories as $cat=>$link){
	for ($i=1; $i <= 2; $i++) { 
		$url = $link.'?page='.$i;

		$html = file_get_html($url);

		$dom =  $html->find('.items')[0]->find('a[target=_blank]');

		foreach($dom as $el) {
			$urls[$el->href] = $cat;
		}
	}
}
foreach ($urls as $key => $value) {
	$obj = new WebParser($key, $value);
	array_push($wpos, $obj);
}


persist($wpos);


