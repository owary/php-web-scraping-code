<?php
function persist($array) // array of web parsers
{

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "cedar";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);
	$conn->set_charset('utf8');

	$articleInsertQuery = "INSERT INTO article (article_title, article_text, article_date, article_url, category) VALUES (?,?,?,?,?)";
	$stmtArticle = $conn->prepare($articleInsertQuery);
	$stmtArticle->bind_param("sssss", $title, $text, $date, $url, $category);

	$imageInsertQuery = "INSERT INTO article_image (article_id, image_url) VALUES (?,?)";
	$stmtImage = $conn->prepare($imageInsertQuery);
	$stmtImage->bind_param("ss", $id, $img);


	$videoInsertQuery = "INSERT INTO article_video (article_id, video_url) VALUES (?,?)";
	$stmtVideo = $conn->prepare($videoInsertQuery);
	$stmtVideo->bind_param("ss", $id, $video);


	$statInsertQuery = "INSERT INTO article_stats (article_id, word_count, sentence_count, most_common_word) VALUES (?,?,?,?)";
	$stmtStat = $conn->prepare($statInsertQuery);
	$stmtStat->bind_param("ssss", $id, $wc, $sc, $mcw);

	$conn->query("START TRANSACTION");
	echo "transaction started"."<br>";
	foreach ($array as $one) {
		echo "Processing - ".$one->title()."<br>";
		$title = $one->title(); 
		$text = $one->text();
		$date = date('Y-m-d',
			strtotime(
				str_replace(".", "/", $one->date())
			));
		$category = $one->category();
		$url = $one->url();
		$stmtArticle->execute();
		// image adding
		$id = mysqli_insert_id($conn);
		foreach ($one->images() as $img) {
			$stmtImage->execute();
		}

		// video adding
		$id = mysqli_insert_id($conn);
		foreach ($one->videos() as $video) {
			$stmtVideo->execute();
		}

		// statistics
		$wc = $one->stats()[0];
		$sc = $one->stats()[1];
		$mcw = $one->stats()[2];
		$stmtStat->execute();

		echo "Processed - ".$one->title()."<br>";
	}
	// closing the connections
	$stmtStat->close();
	$stmtVideo->close();
	$stmtImage->close();
	$stmtArticle->close();

	// commit the changes
	$conn->query("COMMIT");
	echo "transaction finished"."<br>";
}